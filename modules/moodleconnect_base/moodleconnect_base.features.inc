<?php

/**
 * Implementation of hook_node_info().
 */
function moodleconnect_base_node_info() {
  $items = array(
    'moodleconnect_client' => array(
      'name' => t('moodleconnect_client'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titel'),
      'has_body' => '1',
      'body_label' => t('Textkörper'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'moodleconnect_course' => array(
      'name' => t('moodleconnect_course'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titel'),
      'has_body' => '1',
      'body_label' => t('Textkörper'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
