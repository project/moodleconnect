<?php
/**
 * @file
 * Module admin page callbacks.
 */

/**
 * Implements the settings page.
 *
 * @return
 *   The form structure.
 */
function moodleconnect_admin_settings() {

  $form['moodleconnect_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug mode'),
    '#default_value' => variable_get('moodleconnect_debug', 0),
  );

  $form['client']['moodleconnect_client'] = array(
      '#type' => 'textfield',
      '#title' => t('client node type'),
      '#size' => 40,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_client', 'moodleconnect_client'),
      '#description' => t('machine readable name of the node type to manage Moodle server clients.'),
    );
  $form['client']['moodleconnect_client_domain'] = array(
      '#type' => 'textfield',
      '#title' => t('CCK textfield for client domain'),
      '#size' => 40,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_client_domain', 'field_moodleconnect_domain'),
      '#description' => t('machine readable name of the field containing domain name.'),
    );
  $form['client']['moodleconnect_client_protocol'] = array(
      '#type' => 'textfield',
      '#title' => t('CCK integer field fieldname for protocol'),
      '#size' => 40,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_client_protocol', 'field_moodleconnect_protocol'),
      '#description' => t('machine readable fieldname where status is stored in group nodes. This has to be an integer field and single value! Use: 0|http, 1|https'),
    );
  $form['client']['moodleconnect_client_status'] = array(
      '#type' => 'textfield',
      '#title' => t('CCK integer field fieldname for status'),
      '#size' => 40,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_client_status', 'field_moodleconnect_status'),
      '#description' => t('machine readable fieldname where status is stored in group nodes. This has to be an integer field and single value! Use: 0|offline, 1|online'),
    );
  $form['client']['moodleconnect_client_roleid'] = array(
      '#type' => 'textfield',
      '#title' => t('CCK integer field fieldname for default role ID'),
      '#size' => 40,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_client_roleid', 'field_moodleconnect_roleid'),
      '#description' => t('machine readable fieldname where role id is stored in group nodes. This has to be an integer field and single value! Use: 0|offline, 1|online'),
    );

  $form['course']['moodleconnect_course'] = array(
      '#type' => 'textfield',
      '#title' => t('group node type'),
      '#size' => 40,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_course', 'moodleconnect_course'),
      '#description' => t('machine readable name of the node type to manage Moodle groups.'),
    );
  $form['course']['moodleconnect_course_clientnid'] = array(
      '#type' => 'textfield',
      '#title' => t('CCK nodereference fieldname for referenced client NID'),
      '#size' => 40,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_course_clientnid', 'field_moodleconnect_clientnid'),
      '#description' => t('machine readable fieldname where the client NID is stored in course nodes. This has to be a node reference and single value!'),
    );
  $form['course']['moodleconnect_course_id'] = array(
      '#type' => 'textfield',
      '#title' => t('CCK integer fieldname for external course ID'),
      '#size' => 40,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_course_id', 'field_moodleconnect_courseid'),
      '#description' => t('machine readable fieldname where the external course ID is stored in course nodes. This has to be an integer field and single value!'),
    );

  $form['moodleconnect_timelimit'] = array(
      '#type' => 'textfield',
      '#title' => t('time limit for onetime passwords in seconds.'),
      '#size' => 10,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_timelimit', '60'),
      '#description' => t('Special value to trim texts. Dots are automatically added. For a custom block and individual themening you can clone the books of this module.'),
    );
  $form['moodleconnect_auth'] = array(
      '#type' => 'textfield',
      '#title' => t('The authentication method'),
      '#size' => 40,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_auth', 'drupalconnect'),
      '#description' => t('String as defined in moodle systems by the authentication module. Default is "drupalconnect"'),
    );
  $form['moodleconnect_suffix'] = array(
      '#type' => 'textfield',
      '#title' => t('Optional suffix for moodle usernames to avoid conflict.'),
      '#size' => 40,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_suffix', ''),
      '#description' => t('A string which is added to usernames including webservice users. Changing in a running system causes errors. Currently there is no cleaning up for old moodle names in table moodlecnnect_clientusers.'),
    );
  $form['moodleconnect_length'] = array(
      '#type' => 'textfield',
      '#title' => t('trim lengh for block creation'),
      '#size' => 10,
      '#maxlength' => 255,
      '#default_value' => variable_get('moodleconnect_length', '30'),
      '#description' => t('Special value to trim texts. Dots are automatically added. For a custom block and individual themening you can clone the books of this module.'),
    );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}


/**
 * Submit hook for the settings form.
 */
function moodleconnect_admin_settings_submit($form, &$form_state) {
  $op = $form_state['clicked_button']['#value'];
  switch ($op) {
    case t('Save configuration'):
      $values = $form_state['values'];

      // TODO: some validation of the data
      variable_set('moodleconnect_debug', trim($values['moodleconnect_debug']));

      variable_set('moodleconnect_client', trim($values['moodleconnect_client']));
      variable_set('moodleconnect_client_domain', trim($values['moodleconnect_client_domain']));
      variable_set('moodleconnect_client_protocol', trim($values['moodleconnect_client_protocol']));
      variable_set('moodleconnect_client_status', trim($values['moodleconnect_client_status']));
      variable_set('moodleconnect_client_status', trim($values['moodleconnect_client_status']));
      variable_set('moodleconnect_client_roleid', trim($values['moodleconnect_client_roleid']));

      variable_set('moodleconnect_course_clientnid', trim($values['moodleconnect_course_clientnid']));
      variable_set('moodleconnect_course_id', trim($values['moodleconnect_course_id']));

      variable_set('moodleconnect_timelimit', trim($values['moodleconnect_timelimit']));
      variable_set('moodleconnect_auth', trim($values['moodleconnect_auth']));
      variable_set('moodleconnect_suffix', trim($values['moodleconnect_suffix']));
      variable_set('moodleconnect_length', trim($values['moodleconnect_length']));

      break;
    case t('Reset to defaults'):
      $values = $form_state['values'];

      variable_del('moodleconnect_debug');

      variable_del('moodleconnect_client');
      variable_del('moodleconnect_client_domain');
      variable_del('moodleconnect_client_protocol');
      variable_del('moodleconnect_client_status');
      variable_del('moodleconnect_client_roleid');

      variable_del('moodleconnect_course');
      variable_del('moodleconnect_course_clientnid');
      variable_del('moodleconnect_course_id');

      variable_del('moodleconnect_timelimit');
      variable_del('moodleconnect_auth');
      variable_del('moodleconnect_suffix');
      variable_del('moodleconnect_length');

      drupal_set_message(t('The configuration options have been reset to their default values.'));
      break;
      return;
  }
}
