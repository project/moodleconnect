<?php
/**
 * @file
 * Page callbacks for managing MOODLE connections.
 */

/**
 * Menu callback: 'moodleconnect/course/%/add/%'.
 */
function _moodleconnect_page_courseuser_add($coursenid = 0, $uid = 0) {

  if (!(is_numeric($coursenid) && is_numeric($uid))) {
    return _moodleconnect_page_error_arg();
  }

  $node = node_load($coursenid);
  $clientnidfield = variable_get('moodleconnect_course_clientnid', 'field_moodleconnect_clientnid');
  $clientnid = $node->{$clientnidfield}[0]['nid'];

  $clientdata = _moodleconnect_clientdata($clientnid);
  $moodleaccesslevel = _moodleconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($moodleaccesslevel == 0) {
    return _moodleconnect_page_error_access();
  }

  $success = _moodleconnect_courseuser_add($coursenid, $uid);
  if ($success) {
    drupal_set_message(t('User has joined the MOODLE course!'), 'status');
  }
  else {
    drupal_set_message(t('Fail on user joining the MOODLE course!'), 'error');
  }
  drupal_goto('node/' . $coursenid);
  return;
}

/**
 * Menu callback: 'moodleconnect/course/%/rm/%'.
 */
function _moodleconnect_page_courseuser_remove($coursenid = 0, $uid = 0) {
  if (!(is_numeric($coursenid) && is_numeric($uid))) {
    return _moodleconnect_page_error_arg();
  }

  $node = node_load($coursenid);
  $clientnidfield = variable_get('moodleconnect_course_clientnid', 'field_moodleconnect_clientnid');
  $clientnid = $node->{$clientnidfield}[0]['nid'];

  $clientdata = _moodleconnect_clientdata($clientnid);
  $moodleaccesslevel = _moodleconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($moodleaccesslevel == 0) {
    return _moodleconnect_page_error_access();
  }

  $success = _moodleconnect_courseuser_remove($coursenid, $uid);
  if ($success) {
    drupal_set_message(t('User has left the MOODLE course!'), 'status');
  }
  else {
    drupal_set_message(t('Fail on user leaving the MOODLE course!'), 'error');
  }

  drupal_goto('node/' . $coursenid);
  return;
}

/**
 * Menu callback: 'moodleconnect/course/%/rm/%/confirm'.
 */
function _moodleconnect_page_courseuser_remove_confirm($coursenid = 0, $uid = 0) {
  if (!(is_numeric($coursenid) && is_numeric($uid))) {
    return _moodleconnect_page_error_arg();
  }
  global $user;
  $output = '';
  $output .= '<div class="moodleconnect-courseuser-remove-confirm">';
  $account = _moodleconnect_getaccount($uid);
  $coursedata = node_load($coursenid);

  $clientnidfield = variable_get('moodleconnect_course_clientnid', 'field_moodleconnect_clientnid');
  $clientnid = $node->{$clientnidfield}[0]['nid'];

  $clientdata = _moodleconnect_clientdata($clientnid);
  $moodleaccesslevel = _moodleconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($moodleaccesslevel == 0) {
    return _moodleconnect_page_error_access();
  }

  if ($user->uid == $uid) {
    $question = t('Are you sure to leave this MOODLE course?');
  }
  else {
    $question = t('Are you sure to remove this user from this MOODLE course?');
  }
  $output .= '<div class="moodleconnect-list">';
  $output .= '<ul>';
  $output .= '<li class="moodleconnect-listitem-coursename">';
  $output .= t('MOODLE course') . ': ';
  $output .= l($coursedata->title, 'node/' . $coursenid)  .'</li>';
  $output .= '<li class="moodleconnect-listitem-username">';
  $output .= t('username on this system') . ': ';
  $output .= l($account->name, 'user/' . $uid) . '</li>';
  $output .= '<li class="moodleconnect-listitem-clientname">';
  $output .= t('MOODLE client') . ': ' . $clientdata['title'] . '</li>';
  $output .= '<li class="moodleconnect-listitem-domain">';
  $output .= t('MOODLE server domain') . ': ' . $clientdata['domain'] .'</li>';
  $output .= '</ul>';
  $output .= '</div>';
  $output .= '<h3>'. $question .'</h3>';
  $output .= l(t('I understand and confirm.'), 'moodleconnect/course/' . $coursenid . '/rm/'. $uid);
  $output .= '</div>';
  return $output;
}

/**
 * Menu callback: 'moodleconnect/client/%/sso/%'.
 */
function _moodleconnect_page_ssolink($clientnid = 0, $uid = 0) {
  if (!(is_numeric($clientnid) && is_numeric($uid))) {
    return _moodleconnect_page_error_arg();
  }
  $clientdata = _moodleconnect_clientdata($clientnid);
  $moodleaccesslevel = _moodleconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($moodleaccesslevel == 0) {
    return _moodleconnect_page_error_access();
  }
  $account = _moodleconnect_getaccount($uid);
  $moodlesso = _moodleconnect_clientuser_sso($clientnid, $uid);
  $output = '';
  $output .= '<div class="moodleconnect-client-user-sso">';
  $debug = variable_get('moodleconnect_debug', 0);
  if (user_access('moodleconnect administer global') && $debug == 1) {
    $output .= '<div class="moodleconnect-list"><ul>';
    $output .= '<li class="moodleconnect-listitem-clientname">';
    $output .= t('MOODLE client') .': '. $clientdata['title'] .'</li>';
    $output .= '<li class="moodleconnect-listitem-domain">';
    $output .= t('MOODLE server domain') .': '. $clientdata['domain'] .'</li>';
    $output .= '</ul></div>';
  }

  $output .= '<div class="moodleconnect-client-user-sso-link">';
  $output .= '<h3>' . t('MOODLE single sign on') . ':</h3>';
  $output .= '<p>' . t('This is a link for one time use and valid for @timelimit seconds.', array(
    '@timelimit' => variable_get('moodleconnect_timelimit', '60'))) . '</p>';
  if ($moodlesso['op'] == TRUE) {
    $action = $clientdata['protocol'] . $clientdata['domain'] . '/login/index.php';
    $output .= '<form action="' . $action .'" ';
    $output .= 'target="_blank" method="post" name="form" id="form">';

    $output .=  '<input type="hidden" name="username" size="15" value="';
    $output .= $moodlesso['moodlename'] . '" /> <br>';

    $output .= '<input type="hidden" name="password" size="15" value="';
    $output .= $moodlesso['pass'] . '" /> <br>';

    $output .= '<input type="submit" name="Submit" value="';
    $output .= t('moodle login link') . '" /> <br><br>';

    $output .= '</form>';

  }

  $output .= '</div></div>';
  return $output;
}

/**
 * Menu callback: 'moodleconnect/client/%/delete/%'.
 */
function _moodleconnect_page_clientuser_delete($clientnid = 0, $uid = 0) {
  if (!(is_numeric($clientnid) && is_numeric($uid))) {
    return _moodleconnect_page_error_arg();
  }

  $clientdata = _moodleconnect_clientdata($clientnid);
  $moodleaccesslevel = _moodleconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($moodleaccesslevel == 0) {
    return _moodleconnect_page_error_access();
  }

  $db = _moodleconnect_clientuser_delete($clientnid, $uid);
  if ($db) {
    drupal_set_message(t('User deleted from MOODLE client!'), 'status');
  }
  else {
    drupal_set_message(t('Fail on user deleting from MOODLE client!'), 'error');
  }
  drupal_goto('user/' . $uid);
  return;
}

/**
 * Menu callback: 'moodleconnect/client/%/delete/%/confirm'.
 */
function _moodleconnect_page_clientuser_delete_confirm($clientnid = 0, $uid = 0) {
  if (!(is_numeric($clientnid) && is_numeric($uid))) {
    return _moodleconnect_page_error_arg();
  }
  global $user;
  $output = '';
  $output .= '<div class="moodleconnect-clientuser-delete-confirm">';
  $output .= '<div class="moodleconnect-warning, warning">';
  $output .= t('If you confirm all data of the MOODLE client user are deleted on the remote MOODLE server and all MOODLE course entries in this system are deleted!');
  $output .= '</div>';

  $account = _moodleconnect_getaccount($uid);

  // Check if user already clientuser.
  $clientuser = _moodleconnect_clientuser_data($clientnid, $uid);
  $userlogin = $clientuser['moodlename'];

  $clientdata = _moodleconnect_clientdata($clientnid);
  $moodleaccesslevel = _moodleconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($moodleaccesslevel == 0) {
    return _moodleconnect_page_error_access();
  }

  if ($user->uid == $uid) {
    $question = t('Are you sure to delete your MOODLE client account?');
  }
  else {
    $question = t('Are you sure to delete this user from this MOODLE client?');
  }
  $output .= '<div class="moodleconnect-list">';
  $output .= '<ul>';
  $output .= '<li class="moodleconnect-listitem-clientname">';
  $output .= t('MOODLE client') . ': ' . $clientdata['title'] . '</li>';
  $output .= '<li class="moodleconnect-listitem-domain">';
  $output .= t('MOODLE server domain') . ': ' . $clientdata['domain'] . '</li>';
  $output .= '<li class="moodleconnect-listitem-username">';
  $output .= t('username on this system') . ': ';
  $output .= l($account->name, 'user/' . $uid) . '</li>';
  $output .= '<li class="moodleconnect-listitem-moodlename">';
  $output .= t('username on MOODLE client') . ': ' . $userlogin . '</li>';
  $output .= '</ul>';
  $output .= '</div>';

  $output .= '<h3>' . $question . '</h3>';
  $output .= l(t('I understand and confirm this complete deletion.'), 'moodleconnect/client/' . $clientnid . '/delete/' . $uid);
  $output .= '</div>';
  return $output;
}

/**
 * Menu callback: 'moodleconnect/client/%/delete/courses'.
 */
function _moodleconnect_page_client_deletecourses($clientnid = 0) {
  if (!(is_numeric($clientnid))) {
    return _moodleconnect_page_error_arg();
  }
  $clientdata = _moodleconnect_clientdata($clientnid);
  global $user;
  $moodleaccesslevel = _moodleconnect_accesslevel($user->uid, $clientdata['clientuid']);
  if ($moodleaccesslevel < 2) {
    return _moodleconnect_page_error_access();
  }
  _moodleconnect_moodleclientcourses_alldelete($clientnid);
  drupal_set_message(t('courses deleted from MOODLE client!'), 'status');
  drupal_goto('node/' . $clientnid);
  return;
}

/**
 * Menu callback: 'moodleconnect/client/%/delete/courses/confirm'.
 */
function _moodleconnect_page_client_deletecourses_confirm($clientnid = 0) {
  if (!(is_numeric($clientnid))) {
    return _moodleconnect_page_error_arg();
  }
  global $user;
  $output = '';
  $output .= '<div class="moodleconnect-client-deletecourses-confirm">';
  $output .= '<div class="moodleconnect-warning, warning">';
  $output .= t('If you confirm all course data including course nodes of the MOODLE client are deleted!');
  $output .= '</div>';

  // Check if user already clientuser.
  $clientuser = _moodleconnect_clientuser_data($clientnid, $uid);
  $userlogin = $clientuser['moodlename'];

  $clientdata = _moodleconnect_clientdata($clientnid);
  $moodleaccesslevel = _moodleconnect_accesslevel($uid, $clientdata['clientuid']);
  if ($moodleaccesslevel < 2) {
    return _moodleconnect_page_error_access();
  }

  $question = t('Are you sure to delete MOODLE client courses?');

  $output .= '<div class="moodleconnect-list">';
  $output .= '<ul>';
  $output .= '<li class="moodleconnect-listitem-clientname">';
  $output .= t('MOODLE client') . ': ' . $clientdata['title'] .'</li>';
  $output .= '<li class="moodleconnect-listitem-domain">';
  $output .= t('MOODLE server domain') . ': ' . $clientdata['domain'] .'</li>';
  $output .= '</ul>';
  $output .= '</div>';

  $output .= '<h3>' . $question . '</h3>';
  $output .= l(t('I understand and confirm this complete deletion.'), 'moodleconnect/client/' . $clientnid . '/delete/courses');
  $output .= '</div>';
  return $output;
}
/**
 * Menu callback: 'moodleconnect/client/%/delete/users'.
 */
function _moodleconnect_page_client_deleteusers($clientnid = 0) {
  if (!(is_numeric($clientnid))) {
    return _moodleconnect_page_error_arg();
  }
  $clientdata = _moodleconnect_clientdata($clientnid);
  global $user;
  $moodleaccesslevel = _moodleconnect_accesslevel($user->uid, $clientdata['clientuid']);
  if ($moodleaccesslevel < 2) {
    return _moodleconnect_page_error_access();
  }
  $delete = _moodleconnect_moodleclientusers_alldelete($clientnid);
  if ($delete == TRUE) {
    drupal_set_message(t('Users deleted from MOODLE client!'), 'status');
  }
  else {
    drupal_set_message(t('Error on user deletion from MOODLE client!'), 'error');
  }
  drupal_goto('node/' . $clientnid);
  return;
}

/**
 * Menu callback: 'moodleconnect/client/%/delete/users/confirm'.
 */
function _moodleconnect_page_client_deleteusers_confirm($clientnid = 0) {
  if (!(is_numeric($clientnid))) {
    return _moodleconnect_page_error_arg();
  }
  global $user;
  $output = '';
  $output .= '<div class="moodleconnect-client-deleteusers-confirm">';
  $output .= '<div class="moodleconnect-warning, warning">';
  $output .= t('If you confirm all client users including course nodes of the MOODLE client are deleted!');
  $output .= '</div>';

  $clientdata = _moodleconnect_clientdata($clientnid);
  $moodleaccesslevel = _moodleconnect_accesslevel($user->uid, $clientdata['clientuid']);
  if ($moodleaccesslevel < 2) {
    return _moodleconnect_page_error_access();
  }

  $question = t('Are you sure to delete MOODLE client users?');

  $output .= '<div class="moodleconnect-list">';
  $output .= '<ul>';
  $output .= '<li class="moodleconnect-listitem-clientname">';
  $output .= t('MOODLE client') . ': ' . $clientdata['title'] . '</li>';
  $output .= '<li class="moodleconnect-listitem-domain">';
  $output .= t('MOODLE server domain') . ': ' . $clientdata['domain'] . '</li>';
  $output .= '</ul>';
  $output .= '</div>';

  $output .= '<h3>' . $question . '</h3>';
  $output .= l(t('I understand and confirm this complete deletion.'), 'moodleconnect/client/' . $clientnid . '/delete/users');
  $output .= '</div>';
  return $output;
}

/**
 * Return Error Message for page content: argument error.
 * Maybe add an theme function in future.
 */
function _moodleconnect_page_error_arg() {
  drupal_set_message(t('The url arguments are not numeric.'), 'error');
  $output = '';
  $output .= '<hr />';
  $output .= t("There is maybe something wrong with programming");
  $output .= '<hr />';
  return $output;
}

/**
 * Return Error Message for page Content: access error.
 * Maybe add an theme function in future.
 */
function _moodleconnect_page_error_access() {
  drupal_set_message(t('The url arguments are not numeric.'), 'error');
  $output .= '<hr />';
  $output .= t("You don't have access to this page.");
  $output .= '<hr />';
  return $output;
}
