ABOUT
-----
MoodleConnect module allows to integrate drupal with Moodle
(http://moodle.org/) Servers.


FEATURES
--------
+ Special short time password handling to secure REST communications.
+ Drupal users can join MOODLE groups by assigning to group nodes.
+ Moodle client users are automatic created with joining of a Moodle course.
+ MOODLE access can be handles with content access rules maybe with
  organic groups.
+ Single sign on/one time login links as only way to access Moodle server for
  drupal Moodle client users.
+ customize password creation with hook_moodleconnect_passwords()
+ customize actions e.g. status, warning and error with
  hook_moodleconnect_actions()


REQUIREMENTS
------------
CCK module
Services Module with REST API
DrupalConnect Module for each Moodle Server to authenticate the user by drupal:
https://github.com/C-Logemann/moodle-auth_drupalconnect

INSTALLATION
------------
Just add this module packet to the modules folder and enable.
Maybe enable feature module "moodleconnect_base" for node types and cck fields.

CONFIGURATION
-------------
Give guests the permission of "moodleconnect webservice".
There is a default webservice "moodleconnect_rest" you can find at
admin/build/services. The default endpoint path is "moodleconnect-rest"
To use this path the moodle server has to be cofigured like this:
https://drupal.example.com/moodleconnect-rest/moodleconnect/retrieve
You can change add change endpoint paths but maybe it's better to Clone.

If the default user data handling doesn't fit your needs can use
hook_moodleconnect_userfields().
Some settings are changeable through hook_moodleconnect_settings().

Maybe to change the node types for Moodle clients and Moodle courses.
Configure MoodleConnect CCK fields as required. The CCK fields are needed by
the code. You can use the feature "moodleconnect_base" to configure.
For each Moodle Client special configuration has to be done by Moodle
administrators.
Especially REST services and a lot of configuration has to be configured on
each Moodle Server especially for permissions of the REST user via its roles.


USAGE FOR MOODLE ADMINISTRATORS
-------------------------------
1. Create a client node and fill out the special CCK fields:
   + domain name of the MOODLE Server
   + the Role ID on the MOODLE Server for new MOODLE users
   + the status of the client in the system. If offline: the one-time-login
     functionality is blocked
2. Create a special user on the MOODLE client for REST operations with the
    possibility to authentcate via DRUPALconnect Service (see this moodle
    module). The name of the individual REST which is shown on the client node
    view. Also you need the node ID for configuring DRUPALconnect in Moodle.
3. Install and configure DRUPALconnect authentication settings of the client on
    the MOODLE server. Don't forget the patch for webservice auth module.
4. On saving of the client node the internal client data are updated.
    If warnings are shown maybe something is wrong in settings above.
5. Create at least one MOODLE course node per client and fill out the special
    CCK fields:
   + The node reference to the related client node.
   + The course ID on the MOODLE Server.

AUTHOR
------
Carsten Logemann (http://drupal.org/user/218368)
